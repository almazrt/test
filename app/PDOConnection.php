<?php

declare(strict_types=1);

namespace App;

use PDO;

class PDOConnection implements DbConnectionInterface
{
    private $dbConnect;

    public function __construct($dsn, $user, $pass)
    {
        $this->dbConnect = new PDO($dsn, $user, $pass);
        $this->dbConnect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->dbConnect->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    }

    public function execute(string $sql, array $params = []): bool
    {
        $sth = $this->dbConnect->prepare($sql);
        return $sth->execute($params);
    }

    public function query(string $sql, array $params = []): array
    {
        $sth = $this->dbConnect->prepare($sql);
        $sth->execute($params);
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

}
