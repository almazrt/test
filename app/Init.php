<?php

namespace App;

class Init extends Model
{
    const RESULT_NORMAL = 1;
    const RESULT_ILLEGAL = 2;
    const RESULT_FAILED = 3;
    const RESULT_SUCCESS = 4;

    public function __construct(DbConnectionInterface $dbConnection)
    {
        parent::__construct($dbConnection);
        $this->create();
        $this->fill();
    }

    private function create()
    {
        $sql = <<<SQL
create table test
(
	id int auto_increment
		primary key,
	script_name varchar(25) null,
	start_time int null,
	end_time int null,
	result int null
);
SQL;
        return $this->dbConnection->execute($sql);
    }

    private function fill()
    {
        $sql = <<<SQL
insert into test(script_name, start_time, end_time, result) 
values (:script_name, :start_time, :end_time, :result); 
SQL;

        $results = [
            self::RESULT_NORMAL,
            self::RESULT_ILLEGAL,
            self::RESULT_FAILED,
            self::RESULT_SUCCESS
        ];

        $n = 10;

        do {

            $n--;
            $params[':script_name'] = substr(md5(rand(1, 100)), 0, 20);
            $params[':start_time'] = rand(1, 100);
            $params[':end_time'] = rand(1, 100);
            $params[':result'] = $results[array_rand($results, 1)];

        } while ($n > 0 && $this->dbConnection->execute($sql, $params));

    }

    public function get()
    {

        $sql = <<<SQL
select * from test where result in(:result1, :result2);
SQL;

        return $this->dbConnection->query($sql, [
            ':result1' => self::RESULT_NORMAL,
            ':result2' => self::RESULT_SUCCESS,
        ]);
    }

}
