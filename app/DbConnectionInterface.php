<?php

declare(strict_types=1);

namespace App;


interface DbConnectionInterface
{
    public function execute(string $sql, array $params = []): bool;

    public function query(string $sql, array $params = []): array;

}
