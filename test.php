<?php

use App\Init;
use App\PDOConnection;

$loader = require __DIR__ . '/vendor/autoload.php';
$dbConfig = require_once __DIR__ . '/config.php';

$PDOConnection = new PDOConnection("mysql:host={$dbConfig['host']};dbname={$dbConfig['dbname']}", $dbConfig['user'], $dbConfig['password']);

$init = new Init($PDOConnection);

print_r($init->get());
